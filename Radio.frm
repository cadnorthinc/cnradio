VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "mswinsck.ocx"
Object = "{C1A8AF28-1257-101B-8FB0-0020AF039CA3}#1.1#0"; "mci32.ocx"
Begin VB.Form Form1 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Form1"
   ClientHeight    =   8052
   ClientLeft      =   120
   ClientTop       =   744
   ClientWidth     =   4680
   ControlBox      =   0   'False
   Icon            =   "Radio.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8052
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer tmLights 
      Left            =   4140
      Top             =   2400
   End
   Begin VB.TextBox Text1 
      Height          =   1395
      Left            =   0
      MultiLine       =   -1  'True
      TabIndex        =   3
      Text            =   "Radio.frx":0442
      Top             =   1440
      Width           =   3675
   End
   Begin VB.ListBox lstDebug 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.4
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4536
      Left            =   0
      TabIndex        =   5
      Top             =   3240
      Width           =   4635
   End
   Begin MSWinsockLib.Winsock tcpSock 
      Left            =   3720
      Top             =   1980
      _ExtentX        =   593
      _ExtentY        =   593
      _Version        =   393216
   End
   Begin MSComctlLib.Toolbar Toolbar2 
      Align           =   1  'Align Top
      Height          =   396
      Left            =   0
      TabIndex        =   4
      Top             =   396
      Width           =   4680
      _ExtentX        =   8255
      _ExtentY        =   699
      ButtonWidth     =   609
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   8
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   840
      _ExtentX        =   995
      _ExtentY        =   995
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   18
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Radio.frx":0448
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Radio.frx":089A
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Radio.frx":0CEC
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Radio.frx":113E
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Radio.frx":1590
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Radio.frx":19EA
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Radio.frx":1E3C
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Radio.frx":2B16
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Radio.frx":37F0
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Radio.frx":44CA
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Radio.frx":51A4
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Radio.frx":5E7E
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Radio.frx":6B58
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Radio.frx":77AC
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Radio.frx":8400
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Radio.frx":9252
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Radio.frx":A0A4
            Key             =   ""
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Radio.frx":AEF6
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Timer tmSpeak 
      Left            =   3720
      Top             =   2400
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Hang Up"
      Height          =   315
      Left            =   3780
      TabIndex        =   1
      Top             =   2880
      Width           =   855
   End
   Begin MSComctlLib.TabStrip TabStrip1 
      Height          =   315
      Left            =   0
      TabIndex        =   0
      Top             =   2880
      Width           =   3735
      _ExtentX        =   6583
      _ExtentY        =   550
      Style           =   1
      ShowTips        =   0   'False
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   6
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "LN1"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "LN2"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab3 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "LN3"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab4 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "LN4"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab5 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "LN5"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab6 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "LN6"
            ImageVarType    =   2
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   396
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   4680
      _ExtentX        =   8255
      _ExtentY        =   699
      ButtonWidth     =   609
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   8
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin MCI.MMControl Sound 
      Height          =   375
      Left            =   840
      TabIndex        =   7
      Top             =   1560
      Width           =   3540
      _ExtentX        =   6244
      _ExtentY        =   656
      _Version        =   393216
      DeviceType      =   ""
      FileName        =   ""
   End
   Begin VB.Label lblTime 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackColor       =   &H80000007&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Time"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.2
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   420
      Left            =   840
      TabIndex        =   6
      Top             =   960
      Width           =   735
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&Console"
      Begin VB.Menu mnuExitProgram 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuOptions 
      Caption         =   "&Options"
      Begin VB.Menu mnuOptDebug 
         Caption         =   "&Debug"
      End
      Begin VB.Menu mnuOptSeparator1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuOptDirect 
         Caption         =   "Di&rect"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuOptTTS 
         Caption         =   "&TextToSpeach"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuOptVoice 
         Caption         =   "&MS Voice"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuOptSeparator2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuClientSetsVoice 
         Caption         =   "cnRadio Determines Voice"
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuAbout 
         Caption         =   "&About"
      End
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'   Public gIPCServer As String         moved to Sub Main()
Public gFormHeight As Long
Public gActiveChannel As Integer
Public gTransmitting As Boolean
Public gReceiving As Boolean
Public gPlayingSound As Boolean
Public gWaitBeforeReceiving As Integer
Public gWaitToReceive As Variant
Public gSpeechRate As Long
Public gUseXMLPort As Boolean
Public gSpeechVolume As Long
Public gMaxVoices As Integer

Dim gPartialMessage As String
Dim gQueuedForProcessing As String
Dim gButtonCount As Integer

Dim oVoice As Object

Dim oSpeaker() As Object
Dim gRadioEnabled() As Boolean

Dim gPendingRadioQueue(100) As Variant

'   Const INIPath = "Q:\Tritech\VisiCAD\Data\System"
Const INIFile = ""

Const NP_BRIMAC_MESSAGE_CLASS = 9750        '   Brimac Message Class - top level BRIMAC Message Type differentiated by message subtype
Const NP_BSI_OLDINC_NEWINC_XREF = 1         '   Brimac Submessage Type - cross-reference old and new incidents
Const NP_BSI_INITIALIZATION_COMPLETE = 2    '   Brimac Submessage Type - Simulator initialization complete
Const NP_BSI_RADIO_MESSAGE = 3              '   Brimac Submessage Type - Simulator Radio Message
Const NP_BSI_READ_RADIO_SETTINGS = 4        '   Brimac Submessage Type - Read Radio Settings Message
Const NP_BSI_RADIO_SOUNDBYTE = 5            '   Brimac Submessage Type - Play Sound File Message

Private Sub Command1_Click()
    TabStrip1.DeselectAll
End Sub

Private Sub Form_Load()
    Dim n As Integer
    Dim s As Integer
    Dim h As Long
    Dim w As Long
    Dim StartPosition As Variant
    Dim oSpeakerList As Object
    Dim oAudioList As Object
    Dim Success As Boolean
    Dim cError As String
    
    On Error GoTo Form_Load_ERH
    
'    Form1.Show

    frmSplash.LoadStatus = "Loading Voice Fonts"
    
    cError = "Start of Form_Load"
    
    Form1.Caption = "Communications Console  v." & App.Major & "." & App.Minor & " (build " & App.Revision & ")"
    
    h = Screen.Height
    w = Screen.Width
    
    StartPosition = Split(bsiGetSettings("SystemDefaults", "RadioStartupPosition", "0|0", App.Path & "\cnRadio.INI"), "|")
    If Val(StartPosition(1)) > 0 And Val(StartPosition(1)) <= Screen.Width - Form1.Width Then     '   make sure we're on the screen
        Form1.Top = Val(StartPosition(0))
        Form1.Left = Val(StartPosition(1))
    ElseIf Val(StartPosition(1)) < 0 Then    '   if not, then put it on the screen
        Form1.Top = Val(StartPosition(0))
        Form1.Left = 0
    Else
        Form1.Top = Val(StartPosition(0))
        Form1.Left = Screen.Width - Form1.Width
    End If
    
    mnuClientSetsVoice.Checked = (bsiGetSettings("SystemDefaults", "ClientSelectsVoice", "1", gSystemDirectory & "\Simulator.INI") = "1")
    
    Call ReadGlobalSettings
    
    '   Set Voice = New SpVoice
    
    With lstDebug
'        .Font = "Courier New"
        .Clear
        .Visible = False
    End With
    
    Call AddToDebugList(String(80, "-"))
    Call AddToDebugList("*** Startup *** - v" & App.Major & "." & App.Minor & " build " & App.Revision)
    
    cError = "Loading SAPI"
    
    Set oVoice = CreateObject("SAPI.spVoice")
    
    Set oSpeakerList = oVoice.GetVoices
    
    '    For n = 0 To oSpeakerList.Count - 1
    '        Debug.Print oSpeakerList.Item(n).GetDescription
    '        If Left(oSpeakerList.Item(n).GetDescription, 9) = "Microsoft" Then
    '            s = s + 1
    '        End If
    '    Next n
    
    ReDim oSpeaker(0 To 0)
    
    gSpeechVolume = oVoice.Volume
    
    s = 0
    For n = 0 To oSpeakerList.Count - 1                     '   verify that we can load each voice font when we start
        Success = True
        Call AddToDebugList("[INIT] Testing Voice font - " & oSpeakerList.Item(n).GetDescription)
        Set oVoice.Voice = oSpeakerList.Item(n)             '   select the next voice
        oVoice.Volume = 0                                   '   be vewy, vewy quiet
        oVoice.Rate = 10                                    '   talk fast - we don't have all day
        oVoice.Speak "TEST", SVSFlagsAsync
        Do
            DoEvents
        Loop Until oVoice.WaitUntilDone(100)
        If Success Then                                     '   if we're still OK, then save the font in the list
            If s > 0 Then ReDim Preserve oSpeaker(0 To s)
            Call AddToDebugList("[INIT] Voice font ADDED - " & oSpeakerList.Item(n).GetDescription & " in slot " & s)
            Set oSpeaker(s) = oSpeakerList.Item(n)
            s = s + 1
        Else
            Call AddToDebugList("[INIT] Voice font SKIPPED - " & oSpeakerList.Item(n).GetDescription)
        End If
    Next n
    
    gMaxVoices = s
       
    Set oAudioList = oVoice.GetAudioOutputs
    
    If oAudioList.Count > 0 Then
        For n = 0 To oAudioList.Count - 1
            Call AddToDebugList("[INIT] Audio Output Device - " & oAudioList.Item(n).GetDescription)
        Next n
    Else
        Call AddToDebugList("[INIT] No Audio Output Devices found")
    End If
    
    cError = "Setting SOUND attributes"
    
    Sound.Visible = False
    Sound.DeviceType = "WaveAudio"
    Sound.TimeFormat = mciFormatMilliseconds
    Sound.Command = "Open"
    
    cError = "Setting TOOLBAR attributes"
    
    gButtonCount = bsiGetSettings("SETTINGS", "BUTTONCOUNT", -1, App.Path & "\cnRadio.INI")
    If gButtonCount <= 0 Then
        Call bsiPutSettings("SETTINGS", "BUTTONCOUNT", 8, App.Path & "\cnRadio.INI")
        gButtonCount = 8
    End If
    
    Toolbar1.Buttons.Clear
    Toolbar2.Buttons.Clear
    For n = 1 To gButtonCount
        Toolbar1.Buttons.Add
        Toolbar2.Buttons.Add
    Next n
    
    If gButtonCount > 8 Then
        Form1.Width = Form1.Width / 8 * gButtonCount
    End If
    
    Toolbar1.ImageList = ImageList1
    Toolbar2.ImageList = ImageList1
    
    ReDim gRadioEnabled(0 To Toolbar1.Buttons.Count) As Boolean
    
    For n = 1 To Toolbar1.Buttons.Count
        Toolbar1.Buttons(n).Image = 2
        gRadioEnabled(n) = False
    Next n
    
    Toolbar1.Refresh
    
    For n = 1 To Toolbar2.Buttons.Count
        If gButtonCount > 8 Then
            Toolbar2.Buttons(n).Caption = n
        Else
            Toolbar2.Buttons(n).Image = 6 + n
        End If
    Next n
    
'    Toolbar2.Style = tbrFlat
    Toolbar2.Refresh
    
    Form1.Refresh
    
    For n = 1 To TabStrip1.Tabs.Count
        TabStrip1.Tabs(n).Selected = False
    Next n
    
    With lblTime
        .Top = Toolbar1.Top + (Toolbar1.ButtonHeight + Toolbar2.ButtonHeight) + 150
        .Caption = " " & Format(Now, "HH:NN:SS") & " "
        .Left = Form1.Width / 2 - .Width / 2
    End With
    
    With Text1
        .FontSize = 12
        .FontBold = True
        .ForeColor = RGB(255, 0, 0)
        .Left = 25
        .Top = lblTime.Top + lblTime.Height + 20
        .Width = Form1.Width - 170
        .Height = TabStrip1.Top - Text1.Top - 30
        .Text = ""
    End With
    
    lstDebug.Width = Text1.Width
    
'    With TextToSpeech1
'        .Visible = False
'    End With
'
'    With DirectSS1
'        .Visible = False
'    End With
    
    Unload frmSplash
    Form1.Show

    gFormHeight = Form1.Height
    Form1.Height = 4005
    
    TabStrip1.DeselectAll
    
    gTransmitting = False
    gReceiving = False
    
    gWaitToReceive = Now
    
    cError = "Connecting to IPC Service"
    
    Call Connect

    tmSpeak.Enabled = True
    tmSpeak.Interval = 500
    tmLights.Enabled = False
    tmLights.Interval = 100
    
    Exit Sub
    
Form_Load_ERH:
    AddToDebugList "[Error] in Procedure Form_Load: " & Err.Description & " (Error " & Err.Number & ") - " & cError
    Success = False
    Resume Next
End Sub

Private Sub ReadGlobalSettings()
    Call AddToDebugList("Re-reading Settings at " & Format(Now, "HH:nn:ss"))
    gWaitBeforeReceiving = Val(bsiGetSettings("SystemDefaults", "RadioWaitToReceive", "3", gSystemDirectory & "\Simulator.INI"))
    gSpeechRate = Val(bsiGetSettings("SystemDefaults", "RadioSpeechRate", "2", gSystemDirectory & "\Simulator.INI"))
    Call AddToDebugList("   Wait =" & Str(gWaitBeforeReceiving) & "  Rate =" & Str(gSpeechRate))
    gUseXMLPort = IIf(bsiGetSettings("SystemDefaults", "UseXMLPort", "0", gSystemDirectory & "\Simulator.INI") = "1", True, False)
    Call AddToDebugList("   UseXML =" & IIf(gUseXMLPort, "TRUE", "FALSE"))
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Dim Test As Boolean
    
    Test = bsiPutSettings("SystemDefaults", "RadioStartupPosition", Trim(Str(Form1.Top)) & "|" & Trim(Str(Form1.Left)), App.Path & "\cnRadio.INI")
    Test = bsiPutSettings("SystemDefaults", "RadioWaitToReceive", Str(gWaitBeforeReceiving), gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("SystemDefaults", "RadioSpeechRate", Str(gSpeechRate), gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("SystemDefaults", "ClientSelectsVoice", IIf(mnuClientSetsVoice.Checked, "1", "0"), gSystemDirectory & "\Simulator.INI")
    
    Sound.Command = "Close"
    
    Call DisconnectServer
    End
End Sub

Private Sub lstDebug_Click()
    If mnuOptDebug.Checked Then
        Text1.Text = lstDebug.List(lstDebug.ListIndex)
    End If
End Sub

Private Sub mnuAbout_Click()
    Dim aMsg(5) As String
    Dim n As Integer
    
    For n = 1 To UBound(gRadioEnabled)
        If gRadioEnabled(n) Then
            aMsg(3) = Str(n)
            Exit For
        End If
    Next n
    
    If aMsg(3) = Empty Then
        Text1.Text = "The Communications Console is another innovative training aid from CAD North Inc."
    Else
        aMsg(0) = "0003"
        aMsg(1) = "The Communications Console is another innovative training aid from CAD North Inc."
        aMsg(2) = "The Communications Console is another innovative training aid from CAD North Inc."
        aMsg(4) = Int(3 * Rnd)
        
        Call QueueRadioMessage(aMsg)
    End If
End Sub

Private Sub mnuClientSetsVoice_Click()
    If mnuClientSetsVoice.Checked Then
        mnuClientSetsVoice.Checked = False
    Else
        mnuClientSetsVoice.Checked = True
    End If
End Sub

Private Sub mnuExitProgram_Click()
    Unload Form1
End Sub

Private Sub mnuOptDebug_Click()
    If mnuOptDebug.Checked Then
        mnuOptDebug.Checked = False
        Form1.Height = 4005
        lstDebug.Visible = False
    Else
        mnuOptDebug.Checked = True
        Form1.Height = gFormHeight
        lstDebug.Visible = True
    End If
End Sub

Private Sub mnuOptDirect_Click()
    If Not mnuOptDirect.Checked Then
        mnuOptDirect.Checked = True
        mnuOptTTS.Checked = False
        mnuOptVoice.Checked = False
    End If
End Sub

Private Sub mnuOptTTS_Click()
    If Not mnuOptTTS.Checked Then
        mnuOptDirect.Checked = False
        mnuOptTTS.Checked = True
        mnuOptVoice.Checked = False
    End If
End Sub

Private Sub mnuOptVoice_Click()
    If Not mnuOptVoice.Checked Then
        mnuOptDirect.Checked = False
        mnuOptTTS.Checked = False
        mnuOptVoice.Checked = True
    End If
End Sub

Private Sub Sound_Done(NotifyCode As Integer)
    Select Case NotifyCode
        Case mciNotifySuccessful
            Debug.Print "MCI SUCCESS"
        Case mciNotifySuperseded
            Debug.Print "MCI SUPERSEDED"
        Case mciAborted
            Debug.Print "MCI ABORTED"
        Case mciFailure
            Debug.Print "MCI FAILURE"
    End Select
    gPlayingSound = False
    Sound.Command = "Close"
    tmLights.Enabled = False
    Toolbar1.Buttons(gActiveChannel).Image = 1
End Sub

Private Sub TabStrip1_Click()
    Dim Clicked As Integer
    
    Clicked = TabStrip1.SelectedItem.Index
    
    Select Case Clicked
        Case 1
        Case 2
        Case 3
        Case 4
        Case 5
    End Select
    
End Sub

Private Sub tmLights_Timer()
    Static LightOn As Boolean
    
    If gRadioEnabled(gActiveChannel) Then
        LightOn = Not LightOn
        If LightOn Then
            Toolbar1.Buttons(gActiveChannel).Image = 3
        Else
            Toolbar1.Buttons(gActiveChannel).Image = 1
        End If
    End If

End Sub

'---------------------------------------------------------------------------------------
' Procedure :   tmSpeak_Timer
' DateTime  :   4/30/2007 16:40
' Author    :   Brian McGrath
' Copyright :   CAD North Inc. 2006
' Purpose   :
'---------------------------------------------------------------------------------------
'
Private Sub tmSpeak_Timer()
    Dim aMsg As Variant
    Dim nVoice As Integer
    Static LightOn As Boolean
    
    Dim ErrorMsg As String

    On Error GoTo tmSpeak_Timer_ERH

    ErrorMsg = "Start of tmSpeak_Timer"

    lblTime.Caption = " " & Format(Now, "HH:NN:SS") & " "
    
    If IsArray(gPendingRadioQueue(0)) _
        And oVoice.Status.RunningState <> SRSEIsSpeaking _
        And Not gPlayingSound Then
        
        If LightOn Then
            Toolbar1.Buttons(gActiveChannel).Image = 1
            LightOn = False
        End If
        
        aMsg = gPendingRadioQueue(0)
        Call AddToDebugList("[DEQUEUE] >" & aMsg(1))
        
        gActiveChannel = IIf(Val(aMsg(3)) > 0, Val(aMsg(3)), gButtonCount)         '   if channel is missing somehow, default to last valid channel
        
        If gActiveChannel > gButtonCount Then gActiveChannel = gButtonCount     '   if the channel is outside the range of valid radio channels, set it to the last valid channel
        
        If Not mnuClientSetsVoice.Checked Then
            nVoice = Val(aMsg(4))
        Else
            If UBound(aMsg, 1) > 4 Then
                nVoice = Val(aMsg(5)) Mod gMaxVoices
            Else
                nVoice = Val(aMsg(4))
            End If
        End If
        
        If nVoice > UBound(oSpeaker) Then nVoice = UBound(oSpeaker)
        
        If gRadioEnabled(gActiveChannel) Then
        
            If Not gTransmitting And Now >= gWaitToReceive Then

                Debug.Print "[" & Format(Now, "hh:nn:ss") & "] Talking now. Wait time was " & Format(gWaitToReceive, "hh:nn:ss")
                
                Call ShiftPendingQueue                      '   only shift the queue if we are going to speak the message
                
                Select Case Val(aMsg(0))
                    Case NP_BSI_RADIO_MESSAGE               '   Brimac Submessage Type - Simulator Radio Message
                        Text1.Text = aMsg(1)
                        Text1.Refresh
                        If mnuOptTTS.Checked Then
            '                TextToSpeech1.Speak aMsg(2)
                        ElseIf mnuOptDirect.Checked Then
            '                DirectSS1.Speak aMsg(2)
                        ElseIf mnuOptVoice.Checked Then
                            Call AddToDebugList("[SPEAK] Setting Voice to " & oSpeaker(nVoice).GetDescription)
                            Set oVoice.Voice = oSpeaker(nVoice)
                            ErrorMsg = "Setting Rate to " & Str(gSpeechRate)
                            oVoice.Rate = gSpeechRate
                            oVoice.Volume = gSpeechVolume
                            ErrorMsg = "Speaking Phrase: '" & aMsg(2) & "'"
                            oVoice.Speak aMsg(2), SVSFlagsAsync
                            ErrorMsg = "Async Speech initiated"
                            tmLights.Enabled = True
                            Do
                                DoEvents
                            Loop Until oVoice.WaitUntilDone(100)
                            tmLights.Enabled = False
                            Toolbar1.Buttons(gActiveChannel).Image = 1
                        End If
                        gWaitToReceive = DateAdd("S", 1, Now)       '   push out the time to force a wait cycle - was 90 - no idea as to why
                    Case NP_BSI_RADIO_SOUNDBYTE             '   Brimac Submessage Type - Play Sound File Message
                        Debug.Print "SOUND:FILE=" & aMsg(2)
                        Sound.DeviceType = "WaveAudio"
                        Sound.FileName = aMsg(2)
                        Sound.Notify = False
                        Sound.Wait = True
                        Sound.Command = "Open"
                        Debug.Print "SOUND:LENGTH=" & Sound.Length
                        If Sound.Length > 0 Then
                            gPlayingSound = True
                            Sound.Notify = True
                            Sound.Wait = False
                            Sound.Command = "Play"
                            tmLights.Enabled = True
                        Else
                            Debug.Print "SOUND:ERROR=" & Sound.Error
                            Debug.Print "SOUND:ERMSG=" & Sound.ErrorMessage
                            oVoice.Speak "ERROR =" & Sound.Error & " " & " MESSAGE =" & Sound.ErrorMessage, SVSFlagsAsync
                            Sound.Command = "Close"
                        End If
                End Select
            End If
        Else
            Call ShiftPendingQueue      '   message was for an unselected channel - discard
        End If
    End If
    
    If gReceiving Then                  '   if speaking is finished, calculate the pause prior to the next message
        gReceiving = False
        gWaitToReceive = DateAdd("S", gWaitBeforeReceiving, Now)
        Debug.Print "[" & Format(Now, "hh:nn:ss") & "] Setting wait to " & Format(gWaitToReceive, "hh:nn:ss")
    End If
    
    On Error GoTo 0
    Exit Sub

tmSpeak_Timer_ERH:

    Call AddToDebugList("[ERROR] in procedure Timer1_Timer of Form Form1:" & Err.Number & ", " & Err.Description _
                            & " - " & ErrorMsg)
                            
    Resume Next
    
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    Dim nImage As Integer
    
    nImage = Button.Image
    
    Select Case nImage
        Case 1
            Toolbar1.Buttons(Button.Index).Image = 2
            gRadioEnabled(Button.Index) = False
        Case 2
            Toolbar1.Buttons(Button.Index).Image = 1
            gRadioEnabled(Button.Index) = True
    End Select

End Sub

Private Sub Toolbar1_DblClick()
'
End Sub

Private Sub Toolbar2_DblClick()
'
End Sub

Private Sub Toolbar2_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim nButton As Integer
    
    nButton = Int(x / Toolbar2.ButtonWidth) + 1
    
    If nButton <= Toolbar2.Buttons.Count Then
        If Toolbar1.Buttons(nButton).Image <> 2 Then
            gTransmitting = False
            Select Case Button
                Case 1
                    Toolbar1.Buttons(nButton).Image = 1
                Case 2
                    Toolbar1.Buttons(nButton).Image = 1
                Case 3
                    Toolbar1.Buttons(nButton).Image = 1
                Case 4
                    Toolbar1.Buttons(nButton).Image = 1
                Case 5
                    Toolbar1.Buttons(nButton).Image = 1
                Case 6
                    Toolbar1.Buttons(nButton).Image = 1
                Case Else
            End Select
            Text1.Text = ""
        End If
        
        If gButtonCount > 8 Then
            Toolbar2.Buttons(nButton).Image = 0
        Else
            Toolbar2.Buttons(nButton).Image = 6 + nButton
        End If
        
    End If
End Sub

Private Sub Toolbar2_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim nButton As Integer

    nButton = Int(x / Toolbar2.ButtonWidth) + 1
    
    If nButton <= Toolbar2.Buttons.Count Then
        If Toolbar1.Buttons(nButton).Image = 1 Then
            gTransmitting = True
            Select Case Button
                Case 1
                    Toolbar1.Buttons(nButton).Image = 3
                Case 2
                    Toolbar1.Buttons(nButton).Image = 3
                Case 3
                    Toolbar1.Buttons(nButton).Image = 3
                Case 4
                    Toolbar1.Buttons(nButton).Image = 3
                Case 5
                    Toolbar1.Buttons(nButton).Image = 3
                Case 6
                    Toolbar1.Buttons(nButton).Image = 3
                Case Else
            End Select
        Else
            Toolbar2.Buttons(nButton).Image = 6
        End If
    End If
End Sub

Private Sub Connect()
    '   Read all IP Settings each time we try to connect
    
    '   This parameter is now set in Sub Main as a command line parameter
    '   gIPCServer = bsiGetSettings("SystemPreferences", "TCPServer", "**ERROR**", gSystemDirectory & "\Simulator.INI")
    
    If gIPCServer <> "" Then
        Call AddToDebugList("Connecting to IPC Server on " & gIPCServer & " ...")
        If ConnectToServer Then                             '   connecting to IPC Server
            Call AddToDebugList("Connected")
        Else
            Call AddToDebugList("Error on IPC Connect > " & gIPCServer)
        End If
    End If

End Sub

Private Function ConnectToServer() As Boolean
    Dim LocalName As String
    Dim TimeOut As Variant
    
    LocalName = UCase(tcpSock.LocalHostName)
    tcpSock.RemoteHost = gIPCServer
    
    If gUseXMLPort Then
        tcpSock.RemotePort = 122
    Else
        tcpSock.RemotePort = 121
    End If
    
    AddToDebugList "[Connecting to IPC Server] " & tcpSock.RemoteHost & ":" & tcpSock.RemotePort
    
    If tcpSock.State = sckClosed Then
        tcpSock.Connect
        AddToDebugList "[State]" & Str(tcpSock.State) & ", " & GetWinSockState(tcpSock)
        AddToDebugList "[Local port]" & Str(tcpSock.LocalPort)
        
        TimeOut = Now
        
        While tcpSock.State <> sckConnected And DateDiff("S", TimeOut, Now) < 2
            DoEvents
        Wend
        
        If tcpSock.State <> sckConnected Then        '   Timeout
            AddToDebugList "[Error] Failed to connect"
            AddToDebugList "[State]" & Str(tcpSock.State) & ", " & GetWinSockState(tcpSock)
            AddToDebugList "[Closing]"
            tcpSock.Close
            AddToDebugList "[State]" & Str(tcpSock.State) & ", " & GetWinSockState(tcpSock)
            ConnectToServer = False
        Else                                        '   Success!
            AddToDebugList "[State]" & Str(tcpSock.State) & ", " & GetWinSockState(tcpSock)
            AddToDebugList "[Registering] " & LocalName & "_bsiRADIO"
            tcpSock.SendData Chr(2) & Chr(200) & LocalName & "_bsiRADIO" & Chr(3)
            ConnectToServer = True
        End If
    Else
        AddToDebugList "[Error] Cannot connect - invalid state" & Str(tcpSock.State) & ", " & GetWinSockState(tcpSock)
        AddToDebugList "[State]" & Str(tcpSock.State) & ", " & GetWinSockState(tcpSock)
        ConnectToServer = False
    End If

End Function

Private Sub DisconnectServer()
    AddToDebugList "[Disconnecting] " & tcpSock.RemoteHost & ":" & tcpSock.RemotePort
    tcpSock.Close
    AddToDebugList "[State]" & Str(tcpSock.State) & ", " & GetWinSockState(tcpSock)
End Sub

Private Sub AddToDebugList(ByVal sString As String)
    Dim LogItem As String
    Dim fName As String
    Dim fNum As Integer
        
    If Dir(App.Path & "\Logs", vbDirectory) = "" Then
        MkDir App.Path & "\Logs"
    End If
    
    fName = App.Path & "\Logs\cnRadio_" & Format(Now, "YYYY.MM.DD") & ".log"
        
    LogItem = Format(Now, "hh:nn:ss ") & sString
    
    fNum = FreeFile
    
    Open fName For Append As #fNum
    Print #fNum, LogItem
    Close (fNum)
    
    If lstDebug.ListCount > 3000 Then      '   keep the list from overflowing
        lstDebug.RemoveItem 0               '   removing '0' promotes '1' to '0'
    End If
    
    Call lstDebug.AddItem(LogItem)
    
    lstDebug.Selected(lstDebug.ListCount - 1) = True
    
    If Not mnuOptDebug.Checked Then
        lstDebug.ListIndex = lstDebug.ListCount - 1
        lstDebug.Selected(lstDebug.ListIndex) = False
    End If
    
End Sub

Private Function GetWinSockState(oWinSock As Winsock) As String
    ' These constants are already defined in VB6
    '
    ' sckClosed             0 Default. Closed
    ' sckOpen               1 Open
    ' sckListening          2 Listening
    ' sckConnectionPending  3 Connection pending
    ' sckResolvingHost      4 Resolving host
    ' sckHostResolved       5 Host resolved
    ' sckConnecting         6 Connecting
    ' sckConnected          7 Connected
    ' sckClosing            8 Peer is closing the connection
    ' sckError              9 Error
    
    Select Case oWinSock.State
        Case sckClosed                  ' 0
            GetWinSockState = "Closed"
        Case sckOpen                    ' 1
            GetWinSockState = "Open"
        Case sckListening               ' 2
            GetWinSockState = "Listening"
        Case sckConnectionPending       ' 3
            GetWinSockState = "Connection pending"
        Case sckResolvingHost           ' 4
            GetWinSockState = "Resolving host"
        Case sckHostResolved            ' 5
            GetWinSockState = "Host resolved"
        Case sckConnecting              ' 6
            GetWinSockState = "Connecting"
        Case sckConnected               ' 7
            GetWinSockState = "Connected"
        Case sckClosing                 ' 8
            GetWinSockState = "Peer is closing the connection"
        Case sckError                   ' 9
            GetWinSockState = "Error"
    End Select
    
End Function

Private Sub tcpSock_DataArrival(ByVal bytesTotal As Long)
    Dim sTemp As String
    Dim aMessage As Variant
    Dim msgLen As Long
    Dim sReceived As String
    Dim Buffer As String
    Dim cASCII As String
    Dim n As Long, I As Long
    
    Static InProcessNow As Boolean
    
    '   AddToDebugList "[Event] DataArrival" & Str(bytesTotal)
    
    On Error GoTo tcpSock_DataArrival_ERH

    tcpSock.GetData sReceived
    
    gPartialMessage = gPartialMessage & sReceived
    
    If Not InProcessNow Then
    
        InProcessNow = True         '   set flag to avoid calls during the process phase
        
        Buffer = gPartialMessage
        gPartialMessage = ""
        aMessage = Split(Buffer, Chr(vcIP_EOM))     '   split buffer into separate messages at the termination character ASCII 3
        
        Do While UBound(aMessage, 1) > 0
        
            For n = 0 To UBound(aMessage) - 1       '   process everything except the Ubound() element for now
                msgLen = Len(aMessage(n))
                If msgLen > 3 Then
                    aMessage(n) = Right(aMessage(n), msgLen - 1)
                    Call ProcessCADMsg(Left(aMessage(n), 4), Right(aMessage(n), Len(aMessage(n)) - 4))
                Else
                    aMessage(n) = Right(aMessage(n), msgLen - 1)
                    cASCII = ""
                    For I = 1 To Len(aMessage(n))
                        cASCII = cASCII & "[" & Trim(Str(Asc(Mid(aMessage(n), I, 1)))) & "]"
                    Next I
                    AddToDebugList "[ODD THING] >" & aMessage(n) & "< ASCII:" & cASCII
                End If
                
                DoEvents
                
            Next n
            
            Buffer = aMessage(n) & gPartialMessage
            gPartialMessage = ""
            
            aMessage = Split(Buffer, Chr(vcIP_EOM))     '   split buffer into separate messages at the termination character ASCII 3
        
        Loop
        
        If UBound(aMessage, 1) >= 0 Then
            gPartialMessage = aMessage(UBound(aMessage, 1))
        Else
            gPartialMessage = ""
        End If
        
        InProcessNow = False        '   reset flag to allow additional processing
        
    Else
        
        AddToDebugList "[DEBUG] > Received while processing " & Len(gPartialMessage)
        
    End If
    
    Exit Sub
    
tcpSock_DataArrival_ERH:
    AddToDebugList "[ERROR] in tcpSock_DataArrival - " & Err.Description & " (" & Err.Number & ")"
    InProcessNow = False
End Sub

Private Sub tcpSock_Error(ByVal Number As Integer, Description As String, ByVal Scode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, CancelDisplay As Boolean)
    
    AddToDebugList "[Event IPC] Error" & Str(Number) & ": " & Description
    AddToDebugList "[Info]  Source: " & Source & " Scode:" & Str(Scode)
    AddToDebugList "[Info]  HelpFile: " & HelpFile & " HelpContext:" & Str(HelpContext)
    AddToDebugList "[Info]  CancelDisplay: " & Switch(CancelDisplay, "TRUE", Not CancelDisplay, "FALSE")
    AddToDebugList "[State]" & Str(tcpSock.State) & ", " & GetWinSockState(tcpSock)

End Sub

Private Sub ProcessCADMsg(msgType As String, msgBody As String)
    
    Select Case msgType
        Case "9750"         '   NP_BRIMAC_MESSAGE_CLASS
            Call bsiProcessBrimacMessage(msgBody)
    End Select
        
End Sub

Private Sub bsiProcessBrimacMessage(sMessage As String)
    Dim aMsg As Variant
    Dim xItem As ListItem
    Dim n As Integer
    
    aMsg = Split(sMessage, "|")
    
    On Error GoTo ErrHand
    
    Select Case aMsg(0)
        Case Val(NP_BSI_OLDINC_NEWINC_XREF)
'            For n = 0 To UBound(gOldIncInfo)
'                If gOldIncInfo(n).OldID = Val(aMsg(1)) And gOldIncInfo(n).NewID = 0 Then
'                    gOldIncInfo(n).NewID = Val(aMsg(2))
'                    Exit For
'                End If
'            Next n
        Case Val(NP_BSI_INITIALIZATION_COMPLETE)
'            Set xItem = lvEvents.ListItems.Add(, , Format(gScriptTime, "hh:nn:ss"))
'            xItem.SubItems(LVUnit) = ""
'            xItem.SubItems(LVType) = Format(EV_INIT_DONE, "0000")
'            xItem.SubItems(LVDur) = "INITDONE"
'            xItem.SubItems(LVDist) = "INITDONE"
'            xItem.SubItems(LVMessage) = "Scenario Startup: Initialization Complete. Waiting for interface to catch up"
'            xItem.SubItems(LVSort) = Format(gScriptTime, DATE_FORMAT)
        Case Val(NP_BSI_RADIO_MESSAGE)
            Call AddToDebugList("[RADIO] " & sMessage)
            Call QueueRadioMessage(aMsg)
        Case Val(NP_BSI_READ_RADIO_SETTINGS)
            Call AddToDebugList("[SETUP] " & sMessage)
            Call ReadGlobalSettings
        Case Val(NP_BSI_RADIO_SOUNDBYTE)
            Call AddToDebugList("[SOUND] " & sMessage)
            Call QueueSoundByte(aMsg)
    End Select
    
    Exit Sub
    
ErrHand:
    AddToDebugList ("ERROR in bsiProcessBrimacMessage: n=" & Str(n) & ", sMessage=>" & sMessage & "<")
    AddToDebugList ("ERROR in bsiProcessBrimacMessage: " & Str(Err.Number) & ", " & Err.Description)
End Sub

Private Sub QueueRadioMessage(aMessage As Variant)
    Dim n As Integer
    
    n = 0
    
    Do While IsArray(gPendingRadioQueue(n))     '   Scan the queue looking for an empty slot
        n = n + 1
    Loop
    
    Call AddToDebugList("[QUEUED] in slot " & n & " >" & aMessage(1))
    gPendingRadioQueue(n) = aMessage            '   and store the incoming message there until we can play it
    
End Sub

Private Sub ShiftPendingQueue()
    Dim n As Integer
    
    n = 0
    
    For n = 0 To UBound(gPendingRadioQueue) - 1
        If IsEmpty(gPendingRadioQueue(n)) Then Exit For
        
        gPendingRadioQueue(n) = gPendingRadioQueue(n + 1)     '   Shift messages to the head of the queue
        
    Next n

End Sub

Private Sub QueueSoundByte(aMessage As Variant)
    Dim n As Integer
    
    n = 0
    
    Do While IsArray(gPendingRadioQueue(n))     '   Scan the queue looking for an empty slot
        n = n + 1
    Loop
    
    gPendingRadioQueue(n) = aMessage            '   and store the incoming message there until we can play it
    
End Sub
