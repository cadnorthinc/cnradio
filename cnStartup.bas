Attribute VB_Name = "cnStartup"
Const Q_INIPATH = "Q:\Tritech\VisiCAD\Data\System"
Const C_INIPATH = "C:\Tritech\VisiCAD\Data\System"

Public gSystemDirectory As String
Public gIPCServer As String

Sub Main()
    Dim bContinue As Boolean
    Dim aCommandArgs() As String
    Dim nArgc As Integer
    Dim aArgv() As String
    Dim aArg() As String
    Dim Test As Boolean
    Dim cShareLoc As String
    Dim n As Integer
    
    On Error GoTo MAIN_ERH
    
    bContinue = False
    
    Call GetCommandlineArgs(Command$, nArgc, aArgv)
    
    If nArgc > 0 Then                '   there are commandline arguments
        For n = 0 To UBound(aArgv, 1)
            aArg = Split(aArgv(n), "=")
            If Trim(UCase(aArg(0))) = "SHARE" Then
                cShareLoc = Trim(UCase(aArg(1)))
                If cShareLoc = "Q:" Then
                    gSystemDirectory = Q_INIPATH
                ElseIf cShareLoc = "C:" Then
                    gSystemDirectory = C_INIPATH
                '    Else                                        '   incorrect parameter specified on command line
                '        Call MsgBox("TriTech directory structure location missing." & vbCrLf & "Specify SHARE=Q: or SHARE=C: as a command line parameter." & vbCrLf & "Simulator cannot load. Exiting.", vbCritical + vbOKOnly, "CRITICAL ERROR on STARTUP")
                End If
            ElseIf Trim(UCase(aArg(0))) = "IPCSERVER" Then                                            '   incorrect parameter specified on command line
                gIPCServer = Trim(UCase(aArg(1)))
            End If
        Next n
    Else
        gSystemDirectory = Q_INIPATH                    '   default to Q: share
    End If
    
    If Dir(gSystemDirectory & "\System.INI") <> "" And gIPCServer <> "" Then
        bContinue = True
    Else                                            '   incorrect parameter specified on command line
        Call MsgBox("Please specify TriTech Share and IPCServer location on command line." _
                & vbCrLf & " ... SHARE=Q: or SHARE=C: and" _
                & vbCrLf & " ... IPCServer=<IP address> or IPCServer=<Host Name>." & vbCrLf _
                & vbCrLf & "cnRadio cannot load. Exiting.", vbCritical + vbOKOnly, "CRITICAL ERROR on STARTUP")
    End If

    If bContinue Then
        Load frmSplash
        frmSplash.AppName = App.EXEName
        frmSplash.AppVersion = App.Major & "." & App.Minor & " build " & App.Revision
        frmSplash.Show
        Load Form1
    End If
    
    Exit Sub
    
MAIN_ERH:
    Call MsgBox("Please specify TriTech Share and IPCServer location on command line." _
            & vbCrLf & " ... SHARE=Q: or SHARE=C: and" _
            & vbCrLf & " ... IPCServer=<IP address> or IPCServer=<Host Name>." & vbCrLf _
            & vbCrLf & "cnRadio cannot load. Exiting.", vbCritical + vbOKOnly, "CRITICAL ERROR on STARTUP")
End Sub

Private Sub GetCommandlineArgs(cCommandline As String, ByRef nArgc As Integer, ByRef aArgv() As String)
    Dim aParameters() As String
    Dim n As Integer
    
    aParameters = Split(cCommandline, " ")
    
    nArgc = 0
    
    For n = 0 To UBound(aParameters, 1)
        If Len(aParameters(n)) > 0 Then
            ReDim Preserve aArgv(0 To nArgc)
            aArgv(nArgc) = aParameters(n)
            nArgc = nArgc + 1
        End If
    Next n
    
End Sub
